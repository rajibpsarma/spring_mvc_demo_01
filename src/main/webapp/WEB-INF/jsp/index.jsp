<!DOCTYPE HTML>
<html>
<head>
    <title>Sample Spring MVC app</title>
</head>
<body>
	<div>${msg}</div> 
	<br><br>

	Options:
	<ul>
		<li><a href="/addEmployee">Add an Employee</a></li>
		<li><a href="/employees">Get all Employees</a></li>
		<li>Get all Employees sorted on
			<ol>
				<li><a href="/employeesSorted?sortOn=Id">Id</a></li>
				<li><a href="/employeesSorted?sortOn=Name">Name</a></li>
			</ol>
		</li>
		<li>Get an employee by Id <input type="text" id="empId" maxlength="10">
			<button onclick="getEmpById()">Get</button>
		</li>
	</ul>

	<script>
	function getEmpById() {
		var empId = document.getElementById("empId").value;
		location.href = "/employees/" + empId;
	}
	</script>
</body>
</html>