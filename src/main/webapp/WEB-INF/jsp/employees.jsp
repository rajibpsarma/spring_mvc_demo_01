<!DOCTYPE HTML>
<%@page import="org.bitbucket.rajibpsarma.model.Employee"%>
<%@page import="java.util.List"%>
<html>
<head>
    <title>Sample Spring MVC app</title>
    <style type="text/css">
	    tr > th {
	    	text-align: left;
	    }
	    tr > th:first-child {
	    	width: 30%;
	    }
    </style>
</head>
<body>
	<div>${msg}</div><br>
	
	<%
	List<Employee> data = (List<Employee>)request.getAttribute("data");
	if((data.isEmpty())) {
		%>
		No records to show.<br>
		<%
	}
	else {
		%>
		<table>
		<tr><th>Id</th><th>Name</th></tr>
		<%
		for(Employee emp : data) {
			%>
			<tr>
				<td><%=emp.getId()%></td>
				<td><%=emp.getName()%></td>
			</tr>
			<%
		}
		%>
		</table>
		<%
	}
	%>
	
	<br><div><a href="/">Go Home</a></div>
</body>
</html>