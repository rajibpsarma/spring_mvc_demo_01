package org.bitbucket.rajibpsarma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMvcDemo01Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringMvcDemo01Application.class, args);
	}

}