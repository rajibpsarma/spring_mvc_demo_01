package org.bitbucket.rajibpsarma.controller;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.rajibpsarma.model.Employee;
import org.bitbucket.rajibpsarma.service.EmployeeService;
import org.bitbucket.rajibpsarma.service.EmployeeService.SORT_ON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
/*
 * @author Rajib Sarma
 */
public class EmployeeController {
	
	@GetMapping("/")
	public String getWelcomePage(Model model) {
		String msg = "Welcome to the Sample Spring MVC app";
		model.addAttribute("msg", msg);
		return "index";
	}
	
	@Autowired
	private EmployeeService empService;
	
	@GetMapping("/addEmployee")
	public String showViewForAddEmployee() {
		return "addEmployee";
	}
	
	// Add employee
	@PostMapping("/employees")
	public String addEmployee(@ModelAttribute Employee emp, Model model) {
		String msg = "The employee with details " + emp + " has been created.";
		try {
			empService.addEmployee(emp);
		}
		catch(Exception ex) {
			msg = "Error while creating employee with details " + emp;
		}
		model.addAttribute("msg", msg);
		return "result";
	}
	
	
	// get all employees
	@GetMapping("/employees")
	public String getAllEmployees(Model model) {
		String msg = "The details of all the employees with us are as follows :";
		String view = "employees";
		try {
			List<Employee> list = empService.getAllEmployees();
			model.addAttribute("data", list);
		} catch(Exception ex) {
			msg = "Error while fetching all employee details.";
			view = "result";
		}
		model.addAttribute("msg", msg);
		
		return view;
	}
	
	
	// get all employees sorted on id/name
	@GetMapping("/employeesSorted")
	public String getAllSortedEmployees(@RequestParam String sortOn, Model model) {
		String msg = "The details of all the employees with us, sorted on \"" + sortOn + "\" are as follows :";
		String view = "employees";
		
		SORT_ON sort_on = SORT_ON.build(sortOn);
		// If proper value is not passed in sortOn, simply display all the employees
		if(sort_on == null) {
			view = getAllEmployees(model);
		}
		
		// Need to sort
		try {
			List<Employee> list = empService.getAllSortedEmployees(sort_on);
			model.addAttribute("data", list);
		} catch(Exception ex) {
			msg = "Error while fetching all the employees with us, sorted on \"" + sortOn + "\"";
			view = "result";
		}
		model.addAttribute("msg", msg);
		
		return view;
	}
	
	
	// get an employee by id
	@GetMapping("/employees/{id}")
	public String getEmployee(@PathVariable String id, Model model) {
		String msg = "The details of the employee with id \"" + id + "\" is as follows :";
		String view = "employees";
		
		try {
			Employee emp = empService.getEmployeeById(id);
			// emp can be null;
			List<Employee> list = new ArrayList<Employee>();
			if(emp != null) {
				list.add(emp);
			}
			model.addAttribute("data", list);
		} catch(Exception ex) {
			msg = "Error while fetching the details of the employee with id \"" + id + "\"";
			view = "result";
		}
		model.addAttribute("msg", msg);
		
		return view;
	}
}
