package org.bitbucket.rajibpsarma.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.bitbucket.rajibpsarma.model.Employee;
import org.springframework.stereotype.Component;

@Component
/*
 * @author Rajib Sarma
 */
public class EmployeeService {
	private List<Employee> data = new ArrayList<Employee>();
	
	public void addEmployee(Employee emp) {
		data.add(emp);
	}
	
	public List<Employee> getAllEmployees() {
		return data;
	}
	
	public List<Employee> getAllSortedEmployees(SORT_ON sortOn) {
		List<Employee> emp = new ArrayList<Employee>(data);
		Collections.sort(emp, new Comparator<Employee>() {
			@Override
			public int compare(Employee o1, Employee o2) {
				int retVal = 0;
				if(sortOn == SORT_ON.EMPLOYEE_ID) {
					// compare emp ids
					retVal = o1.getId().compareTo(o2.getId());
				} else {
					// compare emp name
					retVal = o1.getName().compareTo(o2.getName());
				}
				return retVal;
			}
		});
		return emp;
	}	
	
	public Employee getEmployeeById(String id) {
		Employee emp = null;
		for(Employee tmp : data) {
			if(tmp.getId().equalsIgnoreCase(id)) {
				// Match found
				emp = tmp;
				break;
			}
		}

		return emp;
	}
	
	public enum SORT_ON {
		EMPLOYEE_ID("id"), EMPLOYEE_NAME("name");
		
		private String sortOn;
		
		private SORT_ON(String sortOn) {
			this.sortOn = sortOn;
		}
		
		public String getSortedOn() {
			return sortOn;
		}
		
		public static SORT_ON build(String sortOn) {
			if("id".equalsIgnoreCase(sortOn)) {
				return EMPLOYEE_ID;
			}
			if("name".equalsIgnoreCase(sortOn)) {
				return EMPLOYEE_NAME;
			}
			return null;
		}
	}
}
