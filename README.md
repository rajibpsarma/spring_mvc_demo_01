# Spring MVC app using boot

It's a demo app that allows to add employees, display all employees, display all employees sorted on their id / name and get an employee based on Id. It uses JSP as view.

## The steps to be followed are: ##
* git clone https://rajibpsarma@bitbucket.org/rajibpsarma/spring_mvc_demo_01.git
* cd spring_mvc_demo_01
* mvn clean spring-boot:run
* Now explore the app using "http://localhost:8080/"






